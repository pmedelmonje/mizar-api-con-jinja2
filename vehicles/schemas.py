import re
from pydantic import BaseModel, Field, validator
from typing import List, Optional, Union
from uuid import UUID
from utils.validators import validate_registration


class ResponseSchema(BaseModel):
    detail: str
    
    
class VehicleBase(BaseModel):
    registration: str = Field(min_length = 6, max_length = 6, 
                            default = "ABDC12 / AB1234")
    brand: str
    model: Optional[str] = None
    color: str
    owner: str
    phone: str = Field(min_length = 12, max_length = 12, 
                       default = "+56912345678")
    

class VehicleCreate(VehicleBase):
    
    pass

    @validator("registration")
    def validate_registration(cls, value):
        if not validate_registration(value):
            raise ValueError("Formato de patente no válido.")
        
        return value
    
    
    @validator("phone")
    def validate_phone_number(cls, value):
        if not re.match(r"^\+?569\d{8}$", value):
            raise ValueError("Formato de número telefónico no válido.")
        
        return value
    

class VehicleUpdate(VehicleCreate):
    pass
    
    @validator("registration")
    def validate_registration(cls, value):
        if not validate_registration(value):
            raise ValueError("Formato de patente no válido.")
        
        return value
    
    
    @validator("phone")
    def validate_phone_number(cls, value):
        if not re.match(r"^\+?569\d{8}$", value):
            raise ValueError("Formato de número telefónico no válido.")
        
        return value
    

class VehicleResponse(VehicleBase):
    id: UUID
    
    class Config:
        orm_mode = True