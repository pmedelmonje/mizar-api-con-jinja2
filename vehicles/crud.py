from uuid import UUID
from sqlalchemy.orm import Session
from .models import Vehicle
from .schemas import *


def list_vehicles(db: Session, skip: int = 0, limit: int = 100):
    return db.query(Vehicle).offset(skip).limit(limit).all()


def get_vehicles_by_brand(db: Session, brand: str):
    return db.query(Vehicle).filter(Vehicle.brand == brand).all()


def get_vehicle_by_id(db: Session, id: UUID):
    return db.query(Vehicle).filter(Vehicle.id == id).first()


def get_vehicle_by_registration(db: Session, registration: str):
    return db.query(Vehicle).filter(Vehicle.registration == registration).first()


def create_vehicle(db: Session, vehicle: dict):
    new_vehicle = Vehicle(**vehicle.dict())
    db.add(new_vehicle)
    db.commit()
    db.refresh(new_vehicle)
    return new_vehicle


def update_vehicle_by_id(db: Session, id: UUID, vehicle_data: dict):
    existing_vehicle = get_vehicle_by_id(db, id)
    if existing_vehicle:
        for field, value in vehicle_data.items():
            setattr(existing_vehicle, field, value)
        db.commit()
        db.refresh(existing_vehicle)
        return existing_vehicle
    return None


def delete_vehicle_by_id(db: Session, id: UUID):
    existing_vehicle = get_vehicle_by_id(db, id)
    if existing_vehicle:
        db.delete(existing_vehicle)
        db.commit()