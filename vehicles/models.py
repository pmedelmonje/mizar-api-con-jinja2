import uuid
import re
from sqlalchemy import Column, String
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship, validates
from settings.db import Base


class Vehicle(Base):
    
    __tablename__ = "vehicle"
    
    id = Column(UUID(as_uuid = True), primary_key = True, 
                unique = True, nullable = False, default = uuid.uuid4)
    registration = Column(String(12), index = True, 
                          unique = True, nullable = False)
    brand = Column(String(64), nullable = False)
    model = Column(String(64), nullable = True, default = "Sin especificar")
    color = Column(String(64), nullable = False)
    owner = Column(String(255), nullable = False)
    phone = Column(String(12), nullable = True, default = "+56912345678")
