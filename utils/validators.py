import re
from typing import Union
from typing_extensions import Annotated
from fastapi import Depends, Header
from fastapi.exceptions import HTTPException
from settings import settings


def validate_token(
    token: Annotated[Union[str, None], Header(alias = "X-API-Key")] = None
):
    
    if token is None:
        raise HTTPException(
            status_code = 401,
            detail = "Se requiere el token 'X-API-Key' como parámetro Header"
        )
        
    if token != settings.master_token:
        raise HTTPException(
            status_code = 403,
            detail = "Token inválido"
        )

    return token


def validate_registration(registration: str):
    registration_regex = re.compile(r'^[BCDFGHJKLMNPRSTVWXYZ]{4}\d{2}|[BCDFGHJKLMNPRSTVWXYZ]{2}\d{4}$') 
    if not registration_regex.match(registration):
        return False
    return True

