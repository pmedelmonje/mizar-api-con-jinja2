from fastapi import APIRouter, Request, Depends
from fastapi.responses import RedirectResponse, HTMLResponse
from fastapi.templating import Jinja2Templates
from settings.db import get_db
from vehicles.crud import *
from vehicles.models import Vehicle


router = APIRouter()
templates = Jinja2Templates(directory = "templates")


@router.get("/")
def root(request: Request):
    return RedirectResponse(router.url_path_for("home_page"))


@router.get("/home")
def home_page(request: Request, db: Session = Depends(get_db)):
    
    vehicles = list_vehicles(db, 0, 1000)
    
    context = {
        "title": "Inicio",
        "request": request,
        "vehicles": vehicles
    }
    return templates.TemplateResponse("web/web_home.html", context)


@router.get("/vehicle-info/{id}")
def vehicle_detail(request: Request, id: UUID, db: Session = Depends(get_db)):
    
    try:
        vehicle = get_vehicle_by_id(db, id)
    except ValueError as e:
        print(str(e))
        return templates.TemplateResponse("web/404.html", {"request": request})
    except Exception as e:
        print(str(e))
        return templates.TemplateResponse("web/404.html", {"request": request})
    
    context = {
        "title": "Información de Vehículo",
        "request": request,
        "vehicle": vehicle
    }

    return templates.TemplateResponse("web/web_vehicle_detail.html", context)